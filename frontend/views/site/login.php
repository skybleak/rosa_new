<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model common\models\LoginForm */

$this->title = 'Добро пожаловать!' ?>
<div class="pb-5 pt-2">
	<div class="d-block d-md-flex justify-content-center mt-5 mx-auto site-login">
		<div class="col-md-6 my-auto pic-entry"></div>
		<div class="col-md-6 login my-4">
			<h1 class="text-center"><?= $this->title ?></h1>
			<?php $form = ActiveForm::begin([
				'id' => 'login-form',
				'enableAjaxValidation' => true,
				'options' => [
					'class' => 'position-relative',
					'autocomplete' => 'off',
				],
				'fieldConfig' => [
					'labelOptions' => ['class' => 'font-18 text-dark mb-1 ml-3'],
				],
			]) ?>

			<?= $form->field($model, 'username', [
				'template' => '{label}<div class="input-group"><div class="input-group-prepend"><div class="input-group-text"><i class="user-icon"></i></div></div>{input}{error}</div>',
			])->textInput(['autofocus' => true, 'class' => 'font-18 text-body form-control']) ?>

			<?= $form->field($model, 'password', [
				'template' => '{label}<div class="input-group"><div class="input-group-prepend"><div class="input-group-text"><i class="lock-icon-mini"></i></div></div>{input}{error}</div>',
			])->passwordInput(['class' => 'font-18 text-body form-control']) ?>

			<div class="row ml-3">
				<?= $form->field($model, 'rememberMe', ['labelOptions' => ['class' => 'custom-control-label']])->checkbox([
					'template' => "<div class='custom-control custom-checkbox mr-auto text-dark'>{input} {label}</div><div class='col-lg-8'>{error}</div>",
					'class' => 'custom-control-input',
				]) ?>
				<a class="ml-auto mr-4 text-info" href="<?= Url::to('request-password-reset') ?>">Забыли пароль?</a>
			</div>

			<div class="d-flex form-group mt-3">
				<?= Html::submitButton('Войти', ['class' => 'btn btn-primary font-14 ml-auto']) ?>
			</div>
			<?php ActiveForm::end() ?>
			<div class="title text-center text-dark">или</div>
			<div class="d-flex mt-4 px-3">
				<p class="font-14 text-dark">Нет аккаунта?</p>
				<a class="ml-auto text-info" href="<?= Url::to('signup') ?>">Зарегистрироваться</a>
			</div>
		</div>
	</div>
</div>