<?php

namespace console\controllers;

use common\models\BicCredit;
use common\models\Currency;
use yii\console\Controller;

class CronController extends Controller
{
    public function actionLoadCurrentRate()
    {
        $currentDate = date("d/m/Y");
        $url = "http://www.cbr.ru/scripts/XML_daily.asp?date_req=" . $currentDate;
        $rates = simplexml_load_string(file_get_contents($url));

        foreach ($rates->children() as $valute) {
            $rate = new Currency();
            $rate->id = (string)$valute['id'];
            $rate->name = (string)$valute->Name;
            $rate->date = Date("yy.m.d");
            $rate->char_code = (string)$valute->CharCode;
            $rate->value = (string)str_replace(",", ".", $valute->Value);
            $rate->save();
        }
    }

    public function actionLoadBicCredit()
    {
        $url = "http://www.cbr.ru/scripts/XML_bic.asp?";
        $bics = simplexml_load_string(file_get_contents($url));

        foreach ($bics->children() as $bicCredit) {
            $bic = new BicCredit();
            $bic->id = (string)$bicCredit['id'];
            $bic->bic = (integer)$bicCredit->Bic;
            $bic->name = (string)$bicCredit->ShortName;
            $bic->save();
        }
    }
}