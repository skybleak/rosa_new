<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%deals}}`.
 */
class m200913_121936_create_user_orgs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_organisation', [
            'user_id' => $this->integer(),
            'organisation_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-user_orgs-user_id-user-id',
            'user_organisation',
            'user_id',
            'user',
            'id'
        );
        $this->addForeignKey(
            'fk-user_orgs-org_id-org-id',
            'user_organisation',
            'organisation_id',
            'organization',
            'id'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_organisation');
    }
}
