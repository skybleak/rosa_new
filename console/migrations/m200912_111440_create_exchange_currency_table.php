<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%exchange_currency}}`.
 */
class m200912_111440_create_exchange_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%exchange_currency}}', [
            'id' => $this->primaryKey(),
            'date' => $this->date(),
            'name' => $this->char(64),
            'char_code' => $this->char(10),
            'currency' => $this->char(50), //убрать!!!!
            'value' => $this->float(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%exchange_currency}}');
    }
}
