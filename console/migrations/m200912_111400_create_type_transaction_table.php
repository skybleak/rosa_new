<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%type_transaction}}`.
 */
class m200912_111400_create_type_transaction_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%type_transaction}}', [
            'id' => $this->primaryKey(),
            'type' => $this->char(30),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%type_transaction}}');
    }
}
