<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%deals}}`.
 */
class m200912_111636_create_deals_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%deals}}', [
            'id' => $this->primaryKey(),
            'report_id' => $this->integer(),
            'order_date' => $this->date(),
            'date_start' => $this->date(),
            'date_end' => $this->date(),
            'rate' => $this->float(),
            'currency_id' => $this->integer(),
            'amount' => $this->float(),
        ]);

        $this->addForeignKey(
            'fk-deals-report_id-report-id',
            'deals',
            'report_id',
            'report',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-deals-currency_id-exchange_currency-id',
            'deals',
            'currency_id',
            'exchange_currency',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%deals}}');
    }
}
