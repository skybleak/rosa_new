<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bic_credit}}`.
 */
class m200912_111520_create_bic_credit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bic_credit}}', [
            'id' => $this->primaryKey(),
            'bic' => $this->integer(11),
            'name' => $this->char(64),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bic_credit}}');
    }
}
