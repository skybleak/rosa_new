<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%account_balance}}`.
 */
class m200912_111620_create_account_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%account_balance}}', [
            'id' => $this->primaryKey(),
            'report_id' => $this->integer(),
            'organization_id' => $this->integer(),
            'bic_id' => $this->integer(),
            'currency_id' => $this->integer(),
            'amount' => $this->float(),
            'comment' => $this->char(255),
        ]);

        $this->addForeignKey(
            'fk-account_balance-report_id-report-id',
            'account_balance',
            'report_id',
            'report',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-account_balance-organization_id-organization-id',
            'account_balance',
            'organization_id',
            'organization',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-account_balance-bic_id-bic_credit-id',
            'account_balance',
            'bic_id',
            'bic_credit',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-account_balance-currency_id-exchange_currency-id',
            'account_balance',
            'currency_id',
            'exchange_currency',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%account_balance}}');
    }
}
