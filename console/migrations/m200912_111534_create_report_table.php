<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%report}}`.
 */
class m200912_111534_create_report_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%report}}', [
            'id' => $this->primaryKey(),
            'date' => $this->date(),
            'organization_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-report-organization_id-organization-id',
            'report',
            'organization_id',
            'organization',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%report}}');
    }
}
