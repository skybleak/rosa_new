<?php

namespace app\helper;

use app\helper\inter\GetParamsInterface;
use app\models\Organization;
use app\models\Report;
use common\models\BicCredit;
use common\models\Currency;
use yii\helpers\ArrayHelper;

class GetParams implements GetParamsInterface
{
    public static function getOrganisation($id)
    {
        return Organization::findOne(['id' => $id]);
    }

    public static function getBIC($id)
    {
        return BicCredit::findOne(['id' => $id]);
    }

    public static function getCurrency($id)
    {
        return Currency::findOne(['id' => $id]);
    }

    public static function getOrganisationForModel()
    {
        $organizations = Organization::find()->all();

        return ArrayHelper::map($organizations, 'id', 'name');
    }

    public static function getBICForModel()
    {
        $bics = BicCredit::find()->all() ;

        return ArrayHelper::map($bics, 'id', 'bic');
    }

    public static function getCurrencyForModel()
    {
        $currences = Currency::find()->all();

        return ArrayHelper::map($currences, 'id', 'name');
    }

    public static function getReport($id)
    {
        return Report::findOne(['id' => $id]);
    }

    public static function getReportForModel()
    {
        $reports = Report::find()->all();

        return ArrayHelper::map($reports, 'id', 'id');
    }
}