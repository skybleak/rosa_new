<?php

namespace app\helper\inter;

interface GetParamsInterface
{
    public static function getOrganisation($id);
    public static function getOrganisationForModel();

    public static function getBIC($id);
    public static function getBICForModel();

    public static function getCurrency($id);
    public static function getCurrencyForModel();

    public static function getReport($id);
    public static function getReportForModel();
}