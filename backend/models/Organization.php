<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organization".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $external_id
 * @property int|null $inn
 * @property int|null $kpp
 * @property int|null $type
 * @property int|null $user_id
 */
class Organization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inn', 'kpp', 'type'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['external_id'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование организации',
            'external_id' => 'External ID',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'type' => 'Type',
            'user_id' => 'User ID',
        ];
    }

    /**
     * {@inheritdoc}
     * @return OrganizationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrganizationQuery(get_called_class());
    }
}
