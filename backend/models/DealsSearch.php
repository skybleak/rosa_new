<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Deals;

/**
 * DealsSearch represents the model behind the search form of `app\models\Deals`.
 */
class DealsSearch extends Deals
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'report_id', 'currency_id'], 'integer'],
            [['order_date', 'date_start', 'date_end'], 'safe'],
            [['rate', 'amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Deals::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'report_id' => $this->report_id,
            'order_date' => $this->order_date,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'rate' => $this->rate,
            'currency_id' => $this->currency_id,
            'amount' => $this->amount,
        ]);

        return $dataProvider;
    }
}
