<?php

namespace app\models;

use common\models\BicCredit;
use common\models\Currency;
use Yii;

/**
 * This is the model class for table "account_balance".
 *
 * @property int $id
 * @property int|null $report_id
 * @property int|null $organization_id
 * @property int|null $bic_id
 * @property int|null $currency_id
 * @property float|null $amount
 * @property string|null $comment
 *
 * @property BicCredit $bic
 * @property ExchangeCurrency $currency
 * @property Organization $organization
 * @property Report $report
 */
class AccountBalance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'account_balance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['report_id', 'organization_id', 'bic_id', 'currency_id'], 'integer'],
            [['amount'], 'number'],
            [['comment'], 'string', 'max' => 255],
            [['bic_id'], 'exist', 'skipOnError' => true, 'targetClass' => BicCredit::className(), 'targetAttribute' => ['bic_id' => 'id']],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
            [['organization_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::className(), 'targetAttribute' => ['organization_id' => 'id']],
            [['report_id'], 'exist', 'skipOnError' => true, 'targetClass' => Report::className(), 'targetAttribute' => ['report_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_id' => 'Report ID',
            'organization_id' => 'Организация',
            'bic_id' => 'БИК',
            'currency_id' => 'Валюта',
            'amount' => 'Сумма',
            'comment' => 'Комментарий',
        ];
    }

    /**
     * Gets query for [[Bic]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBic()
    {
        return $this->hasOne(BicCredit::className(), ['id' => 'bic_id']);
    }

    /**
     * Gets query for [[Currency]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(ExchangeCurrency::className(), ['id' => 'currency_id']);
    }

    /**
     * Gets query for [[Organization]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrganization()
    {
        return $this->hasOne(Organization::className(), ['id' => 'organization_id']);
    }

    /**
     * Gets query for [[Report]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReport()
    {
        return $this->hasOne(Report::className(), ['id' => 'report_id']);
    }
}
