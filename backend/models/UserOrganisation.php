<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_organisation".
 *
 * @property int|null $user_id
 * @property int|null $organisation_id
 */
class UserOrganisation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_organisation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'organisation_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'organisation_id' => 'Organisation ID',
        ];
    }
}
