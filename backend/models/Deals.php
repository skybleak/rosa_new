<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "deals".
 *
 * @property int $id
 * @property int|null $report_id
 * @property string|null $order_date
 * @property string|null $date_start
 * @property string|null $date_end
 * @property float|null $rate
 * @property int|null $currency_id
 * @property float|null $amount
 *
 * @property Report $report
 */
class Deals extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['report_id', 'currency_id'], 'integer'],
            [['order_date', 'date_start', 'date_end'], 'safe'],
            [['rate', 'amount'], 'number'],
//            [['report_id'], 'exist', 'skipOnError' => true, 'targetClass' => Report::className(), 'targetAttribute' => ['report_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_id' => 'Report ID',
            'order_date' => 'Order Date',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'rate' => 'Rate',
            'currency_id' => 'Currency',
            'amount' => 'Amount',
        ];
    }

    /**
     * Gets query for [[Report]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReport()
    {
        return $this->hasOne(Report::className(), ['id' => 'report_id']);
    }
}
