<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $organisations app\models\User */
/* @var $user_organisation app\models\User */
/* @var $org_model app\models\User */

$this->title = 'Update User: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update">
    <?= $this->render('../layouts/_nav') ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <h2>Организации пользователя</h2>
    <?= $this->render('_userorgform', [
        'model' => $model,
        'organisations' => $organisations,
        'user_organisation' => $user_organisation,
        'org_model' => $org_model
    ]) ?>
</div>
