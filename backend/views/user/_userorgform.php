<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
/* @var $organisations app\models\User */
/* @var $user_organisation app\models\User */
/* @var $org_model app\models\User  */

$params = [
    'prompt' => 'Выберите организацию...'
];

?>



    <?php
    echo '';
    if(count($user_organisation)){
        foreach ($user_organisation as $user_org){
            echo '<label><input type="text" class="form-control col-md-9"  value="'.$organisations[$user_org['organisation_id']].'" aria-invalid="false">';
            echo '<a href="/lk/user/organisation_delete?id='.$user_org['organisation_id'].'" title="Delete" aria-label="Delete"  data-confirm="Are you sure you want to delete this item?" data-method="post">удалить</span></a></label>';
        }
    }
    ?>

<div class="user-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($org_model, 'id')->dropDownList($organisations,$params); ?>


    <div class="form-group">
        <?= Html::submitButton('Добавить организацию', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
