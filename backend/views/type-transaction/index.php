<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TypeTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Type Transactions';
?>
<div class="type-transaction-index">

    <?= $this->render('../layouts/_nav') ?>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	<p>
		<?= Html::a('Create Type Transaction', ['create'], ['class' => 'btn btn-success']) ?>
	</p>


</div>
