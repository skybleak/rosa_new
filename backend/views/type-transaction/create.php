<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TypeTransaction */

$this->title = 'Create Type Transaction';
$this->params['breadcrumbs'][] = ['label' => 'Type Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-transaction-create">
    <?= $this->render('../layouts/_nav') ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
