<?php

use app\models\Currency;
use app\models\Report;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Deals */
/* @var $form yii\widgets\ActiveForm */
/* @var $reports array */
/* @var $currences Currency */
?>

<div class="deals-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'report_id')->dropDownList($reports, ['Выбирает']) ?>

    <?= $form->field($model, 'order_date')->widget(DatePicker::class, ([
        'name' => 'check_issue_date',
        'value' => date('d-M-Y', strtotime('+2 days')),
        'options' => ['placeholder' => 'Select issue date ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ])); ?>


    <?= $form->field($model, 'date_start')->widget(DatePicker::class, ([
        'name' => 'check_issue_date',
        'value' => date('d-M-Y', strtotime('+2 days')),
        'options' => ['placeholder' => 'Select issue date ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ])); ?>

    <?= $form->field($model, 'date_end')->widget(DatePicker::class, ([
        'name' => 'check_issue_date',
        'value' => date('d-M-Y', strtotime('+2 days')),
        'options' => ['placeholder' => 'Select issue date ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ])); ?>

    <?= $form->field($model, 'rate')->textInput() ?>

    <?= $form->field($model, 'currency_id')->widget(Select2::classname(), [
        'data' => $currences,
        'language' => 'ru',
        'options' => ['placeholder' => 'Выберите валюту'],
        'pluginOptions' => [
            'allowClear' => true
        ],
        'disabled' => $disabled
    ]); ?>
    <?= $form->field($model, 'amount')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
