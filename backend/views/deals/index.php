<?php

use app\helper\GetParams;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DealsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сделки';
echo $this->render('../layouts/_nav');
?>
<div class="deals-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать сделку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'order_date',
            'date_start',
            'date_end',
            'rate',
            [
                'attribute' => 'currency_id',
                'header' => 'Валюта',
                'value' => function ($data) {
                    $currency = GetParams::getCurrency($data->currency_id);

                    return $currency->name;
                },
            ],
            'amount',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
