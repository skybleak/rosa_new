<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrganizationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Organizations';
?>
<div class="organization-index">

    <?= $this->render('../layouts/_nav') ?>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'external_id',
            'inn',
            'kpp',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

	<p>
		<?= Html::a('Create Organization', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
</div>
