<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Organization */

$this->title = 'Просмотр остатка на счету';
$this->params['breadcrumbs'][] = ['label' => 'AccountBalance', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_form',   [
    'model' => $model,
    'view' => true,
]);?>