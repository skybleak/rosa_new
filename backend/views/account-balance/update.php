<?php

use app\models\BicCredit;
use app\models\Currency;
use app\models\Organization;

/* @var $organizations Organization */
/* @var $bics BicCredit */
/* @var $currences Currency */

$this->title = 'Изменение остатка на счету';
$this->params['breadcrumbs'][] = ['label' => 'AccountBalance', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form',   [
    'model' => $model,
    'organizations' => $organizations,
    'bics' => $bics,
    'currences' => $currences,
]);?>
