<?php
/* @var $this yii\web\View */
/* @var $searchModel app\models\AccountBalanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $data Organization */

echo $this->render('../layouts/_nav');

use app\helper\GetParams;
use app\models\Organization;
use yii\bootstrap4\Html;
use yii\grid\GridView;

?>
<div class="account-balance-index">
    <p>
        <?= Html::a('Добавление остатка на счету', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'report_id',
            [
                'attribute' => 'organization_id',
                'header' => 'Организация',
                'value' => function ($data) {
                    $org = GetParams::getOrganisation($data->organization_id);

                    return $org->name;
                },
            ],
            [
                'attribute' => 'bic_id',
                'header' => 'БИК',
                'value' => function ($data) {
                    $bic = GetParams::getBIC($data->bic_id);

                    return $bic->bic;
                },
            ],
            [
                'attribute' => 'currency_id',
                'header' => 'Валюта',
                'value' => function ($data) {
                    $currency = GetParams::getCurrency($data->currency_id);

                    return $currency->name;
                },
            ],
            [
                'attribute' => 'amount',
                'header' => 'Сумма',
                'value' => $data->amount
            ],
            [
                'attribute' => 'comment',
                'header' => 'Комментарий',
                'value' => $data->comment
            ],

            [
                'header' => 'Дата',
                'value' => function ($data) {
                    $report = GetParams::getReport($data->report_id);

                    return $report->date;
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>


</div>
