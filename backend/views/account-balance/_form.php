<?php

use app\models\BicCredit;
use app\models\Currency;
use app\models\Organization;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/* @var $organizations Organization */
/* @var $bics BicCredit */
/* @var $currences Currency */
/* @var $view bool */

$disabled = $view ? true : false;
?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'method' => 'post',
    'class' => 'form-inline',
]); ?>

<?= $form->field($model, 'organization_id')->widget(Select2::classname(), [
    'data' => $organizations,
    'language' => 'ru',
    'options' => ['placeholder' => 'Выберите организацию'],
    'pluginOptions' => [
        'allowClear' => true
    ],
    'disabled' => $disabled
]); ?>

<?= $form->field($model, 'bic_id')->widget(Select2::classname(), [
    'data' => $bics,
    'language' => 'ru',
    'options' => ['placeholder' => 'Выберите Банковский идентификационный код'],
    'pluginOptions' => [
        'allowClear' => true
    ],
    'disabled' => $disabled
]); ?>

<?= $form->field($model, 'currency_id')->widget(Select2::classname(), [
    'data' => $currences,
    'language' => 'ru',
    'options' => ['placeholder' => 'Выберите валюту'],
    'pluginOptions' => [
        'allowClear' => true
    ],
    'disabled' => $disabled
]); ?>

<?= $form->field($model, 'amount')->textInput([
    'disabled' => $disabled
]) ?>

<?= $form->field($model, 'comment')->textarea([
    'disabled' => $disabled
]) ?>

<?php if (!$view): ?>
    <div class="form-group col text-center">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
    </div>
<?php endif; ?>

<?php ActiveForm::end(); ?>

