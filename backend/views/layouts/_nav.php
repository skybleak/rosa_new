<?php


use yii\bootstrap4\Tabs;

$menuItems = [
    ['label' => 'Организации', 'url' => ['/organisation/index']],
    ['label' => 'Типы сделок', 'url' => ['/type-transaction/index']],
    ['label' => 'Пользователи', 'url' => ['/user/index']],
    ['label' => 'Остатки на счетах', 'url' => ['/account-balance/index']],
    ['label' => 'Сделки', 'url' => ['/deals/index']],
    ['label' => 'Оплата', 'url' => ['/type-transaction/index']],
    ['label' => 'Создать документ', 'url' => ['/document/index']],
];
echo Tabs::widget([
  'items' =>
    $menuItems
]);
?>

