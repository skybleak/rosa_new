<?php

namespace backend\controllers;

use app\helper\GetParams;
use app\models\AccountBalance;
use app\models\AccountBalanceSearch;
use app\models\Organization;
use app\models\OrganizationSearch;
use app\models\Report;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class AccountBalanceController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new AccountBalanceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new AccountBalance();

        if ($model->load(Yii::$app->request->post())) {
            $report = new Report();
            $report->date = date("Y-m-d");
            $report->organization_id = $model->organization_id;
            $report->save();

            $model->report_id = $report->id;
            $model->save();

            return $this->redirect('index');
        }

        return $this->render('create',
            [
                'model' => $model,
                'organizations' => GetParams::getOrganisationForModel(),
                'bics' => GetParams::getBICForModel(),
                'currences' => GetParams::getCurrencyForModel(),
            ]
        );
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('index');
        }

        return $this->render('update',
            [
                'model' => $model,
                'organizations' => GetParams::getOrganisationForModel(),
                'bics' => GetParams::getBICForModel(),
                'currences' => GetParams::getCurrencyForModel(),
            ]
        );
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view',
            [
                'model' => $model,
                'view' => true,
            ]
        );
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = AccountBalance::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
