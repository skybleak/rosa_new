<?php

namespace backend\controllers;

use yii\filters\AccessControl;
use yii\web\Controller;

class DocumentController extends Controller
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['index'],
						'roles' => ['user'],
					],
				],
			],
		];
	}

	public function actionIndex()
	{
		return $this->render('index');
	}
}