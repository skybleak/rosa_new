<?php

use yii\web\GroupUrlRule;

return [
    '/' => 'site/index',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
    'type-transaction/<action:\w+>' => 'type-transaction/<action>',
    'account-balance/<action:\w+>' => 'account-balance/<action>',
];
