<?php

use yii\BaseYii;
use yii\caching\CacheInterface;

/**
 * Yii bootstrap file.
 * Used for enhanced IDE code autocompletion.
 */
class Yii extends BaseYii
{
	/**
	 * @var BaseApplication the application instance
	 */
	public static $app;
}

/**
 * Class BaseApplication
 * Used for properties that are identical for both WebApplication and ConsoleApplication
 *
 * @property yii\web\UrlManager $urlManagerBackend
 * @property yii\web\UrlManager $urlManagerFrontend
 * @property yii\i18n\Formatter $intFormatter
 * @property yii\i18n\Formatter $dayDBFormatter
 * @property CacheInterface $cacheBackend
 * @property CacheInterface $cacheFrontend
 * @property string $name
 */
abstract class BaseApplication extends yii\web\Application
{
}
