<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bic_credit".
 *
 * @property int $id
 * @property int|null $bic
 * @property string|null $name
 *
 * @property AccountBalance[] $accountBalances
 */
class BicCredit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bic_credit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bic'], 'integer'],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bic' => 'Bic',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[AccountBalances]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAccountBalances()
    {
        return $this->hasMany(AccountBalance::className(), ['bic_id' => 'id']);
    }
}
