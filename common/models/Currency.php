<?php

namespace common\models;

/**
 * This is the model class for table "exchange_currency".
 *
 * @property int $id
 * @property string|null $date
 * @property string|null $name
 * @property string|null $char_code
 * @property string|null $currency
 * @property string|null $value
 *
 * @property AccountBalance[] $accountBalances
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exchange_currency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['name', 'value'], 'string', 'max' => 64],
            [['char_code'], 'string', 'max' => 10],
            [['currency'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'name' => 'Name',
            'char_code' => 'Char Code',
            'currency' => 'Currency',
            'value' => 'Value',
        ];
    }

    /**
     * Gets query for [[AccountBalances]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAccountBalances()
    {
        return $this->hasMany(AccountBalance::className(), ['currency_id' => 'id']);
    }
}
